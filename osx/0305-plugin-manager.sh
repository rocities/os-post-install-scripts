#!/bin/sh

install() {
  # install dein.vim [https://github.com/Shougo/dein.vim]
  curl -s https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh | bash -s ~/.vim/bundles
  # :call dein#install()
  nvim -c 'call dein#install() | q'
}

update() {
  nvim -c 'call dein#install() | q'
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  update
elif [ "$1" = "uninstall" ]; then
  echo "skipping $1 on $0"
else
  echo "unsupported action $1 on $0"
fi
