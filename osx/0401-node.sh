#!/bin/sh

install() {
  brew install node -y
}

update() {
  brew upgrade node -y
}

uninstall() {
  brew uninstall node -y
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  update
elif [ "$1" = "uninstall" ]; then
  uninstall
else
  echo "unsupported action $1 on $0"
fi
