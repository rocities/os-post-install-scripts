# Check $GL_DIR
# Run with `. install.sh`
if [ -z $GL_DIR ]; then
  echo "\$GL_DIR environment variable is not set. Terminating script."
  exit 1
fi

for i in `ls +([0-9])*.sh`; do
  . $i install
done
