#!/bin/sh

install() {
  # https://gitlab.com/rocities/tmux-config
  brew install tmux -y
}

update() {
  brew upgrade tmux -y
}

uninstall() {
  brew uninstall tmux -y
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  update
elif [ "$1" = "uninstall" ]; then
  uninstall
else
  echo "unsupported action $1 on $0"
fi
