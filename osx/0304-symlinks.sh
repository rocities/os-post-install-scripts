#!/bin/sh

install() {
  # create directory for nvim config file
  mkdir -p ~/.config/nvim/
  # create symlink for init.vim
  (cd $GL_DIR/nvim-config && ln -s $(pwd)/init.vim ~/.config/nvim/init.vim)
}

uninstall() {
  rm -f ~/.config/nvim/init.vim
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  echo "skipping $1 on $0"
elif [ "$1" = "uninstall" ]; then
  uninstall
else
  echo "unsupported action $1 on $0"
fi
