#!/bin/sh

install() {
  brew install nvim -y
}

update() {
  brew upgrade nvim -y
}

uninstall() {
  brew uninstall nvim -y
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  update
elif [ "$1" = "uninstall" ]; then
  uninstall
else
  echo "unsupported action $1 on $0"
fi
