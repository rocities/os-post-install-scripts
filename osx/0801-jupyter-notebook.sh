#!/bin/sh

install() {
  pip3 install jupyter-notebook
}

update() {
  pip3 install jupyter-notebook --upgrade
}

uninstall() {
  pip3 uninstall jupyter-notebook
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  update
elif [ "$1" = "uninstall" ]; then
  uninstall
else
  echo "unsupported action $1 on $0"
fi
