#!/bin/sh

install() {
  brew install python -y
}

update() {
  brew upgrade python -y
}

uninstall() {
  brew uninstall python -y
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  update
elif [ "$1" = "uninstall" ]; then
  uninstall
else
  echo "unsupported action $1 on $0"
fi
